###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Taken from
# https://gitlab.cern.ch/lhcb-dirac/LHCbWebDIRAC/-/blob/master/WebApp/handler/AnalysisProductionsHandler.py
#
# Simple tool to dump the list of files for a given Analysis Production, filtering them by magnet polarity and year.
#

import argparse
import json
import os
import re
import sys
import json

from DIRAC.Core.Base.Script import parseCommandLine

# Necessary DIRAC setup
sys.argv, backup_argv = sys.argv[:1], sys.argv
parseCommandLine()
sys.argv = backup_argv

from LHCbDIRAC.BookkeepingSystem.Client.BookkeepingClient import BookkeepingClient

if __name__ == "__main__":
    bk = BookkeepingClient()
    bk.setTimeout(120)
    sfilter = '{"ApplicationName": [["Gauss"], ["Boole"], ["Moore"], ["Brunel"], ["DaVinci"], ["LHCb"], ["mergeMDF"], ["Noether"], ["Tesla"], ["Erasmus"], ["Castelao"]], "Visible": [["Yes"], ["No"]], "Usable": [["Yes"], ["Not ready"], ["Obsolete"]]}'
    result = bk.getAvailableSteps({})

    with open("allsteps.json", "w") as f:
        json.dump(result, f) 


