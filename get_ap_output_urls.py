###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Taken from
# https://gitlab.cern.ch/lhcb-dirac/LHCbWebDIRAC/-/blob/master/WebApp/handler/AnalysisProductionsHandler.py
#
# Simple tool to dump the list of files for a given Analysis Production, filtering them by magnet polarity and year.
#

import argparse
import json
import os
import re
import sys

from DIRAC.Core.Base.Script import parseCommandLine

# Necessary DIRAC setup
sys.argv, backup_argv = sys.argv[:1], sys.argv
parseCommandLine()
sys.argv = backup_argv

from DIRAC import gConfig, gLogger
from LHCbDIRAC.ProductionManagementSystem.Client.ProductionRequestClient import (
    ProductionRequestClient,
)
from LHCbDIRAC.BookkeepingSystem.Client.BookkeepingClient import BookkeepingClient
from DIRAC.TransformationSystem.Client.TransformationClient import TransformationClient
from LHCbDIRAC.Core.Utilities.JSONPickle import pickleOrJsonLoads
from DIRAC.Core.Workflow.Workflow import fromXMLString as workflowFromXMLString
from DIRAC.ConfigurationSystem.Client.Helpers.Operations import Operations
from DIRAC.Resources.Storage.StorageElement import StorageElement
from DIRAC.DataManagementSystem.Client.DataManager import DataManager


from collections import defaultdict
from functools import reduce
from pprint import pprint

KNOWN_DATAPKGS = ["AnalysisProductions", "WG/CharmWGProd", "WG/CharmConfig"]
DONE_TRANSFORMATION_STATES = ["Archived", "Deleted", "Cleaned", "Completed"]
RE_PACKAGE_VERSION = re.compile(r"^v(?P<major>\d+)r(?P<minor>\d+)(?:p(?P<patch>\d+))?$")


def _unwrap(result):
    """Unwrap an S_OK/S_ERROR response, raising if appropriate"""

    if not result["OK"]:
        raise ValueError(str(result))
    return result["Value"]


def getInputPath(production):
    """Takes the production as a dictionary as returned by productionRequestClient().getProductionRequestList,
    and returns the BK path
    # Taken from WebApp/handler/AnalysisProductionsHandler.py as not shared
    """
    simCondDetail = getSimCondDetail(production)
    productionInputQuery = os.path.join(
        "/",
        simCondDetail["configName"],
        simCondDetail["configVersion"],
        production["SimCondition"],
        simCondDetail["inProPass"],
        production["EventType"],
        simCondDetail["inFileType"],
    )
    return productionInputQuery


def getSimCondDetail(production):
    """Load the simCondDetails"""
    return pickleOrJsonLoads(production["SimCondDetail"])


def _getBKQuery(production):
    """Takes the production as a dictionary as returned by productionRequestClient().getProductionRequestList,
    and returns the dict needed to gquery more info from those conditions,
    e.g. # {"ConfigVersion":"Collision18","ConfigName":"LHCb","ConditionDescription":"Beam6500GeV-VeloClosed-MagDown"}
    """
    simCondDetail = pickleOrJsonLoads(production["SimCondDetail"])
    ret = dict()
    ret["ConfigVersion"] = simCondDetail["configVersion"]
    ret["ConfigName"] = simCondDetail["configName"]
    ret["ConditionDescription"] = production["SimCondition"]
    return ret


def getConditions(production):
    """Query the bookkeeping to have more information about the conditions for the production
    returns a pair  (condType, conditions), where condType is sim or daq,
    and conditions a dict like
    {'G4settings': 'specified in sim step', 'BeamEnergy': '6500 GeV', 'Description': 'Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8', 'Generator': 'Pythia8', 'Luminosity': 'pp collisions nu = 1.6, 25ns spillover',
    'MagneticField': '-1', 'DetectorCondition': '2016, Velo closed around average x=0.84mm and y=-0.18mm', 'BeamCondition': 'beta*~3m, zpv=-3.1mm, xAngle=-0.395mrad and yAngle=0', 'SimId': 433723}
    """

    bkQuery = _getBKQuery(production)
    result = BookkeepingClient().getConditions(bkQuery)
    if not result["OK"]:
        raise ValueError(str(result))

    records = result["Value"]
    conditions = []
    if records[0]["TotalRecords"] > 0:
        paramNames = records[0]["ParameterNames"]
        for rec in records[0]["Records"]:
            conditions = dict(zip(paramNames, rec))
            if bkQuery["ConditionDescription"] in conditions.viewvalues():
                condType = "sim"
                break
    else:
        paramNames = records[1]["ParameterNames"]
        for rec in records[1]["Records"]:
            conditions = dict(zip(paramNames, rec))
            if bkQuery["ConditionDescription"] in conditions.viewvalues():
                condType = "daq"
                break

    return (condType, conditions)


def transformationToDict(transformation):
    """Convert the response of getTransformations to a dictionary

    :param production: A row returned from getTransformations
    :returns: A dictionary summarising this transformation
    """
    transformationID = transformation["TransformationID"]
    transformationStatus = transformation["TransformationID"]

    workflow = workflowFromXMLString(transformation["Body"])
    parameters = {p.getName(): p.getValue() for p in workflow.parameters}

    # Skip this if the transfromation is done as it's relatively slow
    fileCounts = {}
    if transformationStatus not in DONE_TRANSFORMATION_STATES:
        fileCounts = _unwrap(
            TransformationClient().getTransformationFilesCount(
                transformationID, "Status"
            )
        )

    logsURL = getTransformationLogsURL(
        parameters["configName"], parameters["configVersion"], transformationID
    )

    steps = [
        stepToDict(parameters["BKProcessingPass"]["Step" + str(i)])
        for i in range(len(parameters["BKProcessingPass"]))
    ]

    transformInputQuery = _unwrap(
        TransformationClient().getBookkeepingQuery(transformationID)
    )

    return {
        "transformationID": transformationID,
        "transformationStatus": transformation["Status"],
        "transformationType": transformation["Type"],
        "fileCounts": fileCounts,
        "logsURL": logsURL,
        "steps": steps,
        "transformInputQuery": transformInputQuery,
    }


def getTransformationLogsURL(configName, configVersion, transformationID):
    """Get the HTTPS URL on logSE for a given transformation

    :param configName: The transformations config name
    :param configVersion: The transformations config version
    :param transformationID: The ID of the transformation
    :returns: A string corresponding to the base URL of the logs
    """
    from LHCbDIRAC.Core.Utilities.ProductionData import _makeProductionPath, _getLFNRoot

    lfnRoot = _getLFNRoot("", configName, configVersion)
    lfn = _makeProductionPath("", lfnRoot, "LOG", str(transformationID).zfill(8))
    logSE = Operations().getValue("LogStorage/LogSE", "LogSE")
    result = StorageElement(logSE).getURL(lfn, protocol="https")
    return _unwrap(result)["Successful"][lfn]


def stepToDict(stepData):
    """Convert a BKProcessingPass step to a dictionary

    :param production: A step from the BKProcessingPass parameter of a workflow
    :returns: A dictionary summarising this step
    """
    extras = stepData["ExtraPackages"].split(";")
    for extra in extras:
        if any(extra.startswith(package) for package in KNOWN_DATAPKGS):
            package, version = extra.split(".")
            if not RE_PACKAGE_VERSION.match(version):
                raise NotImplementedError(package, version)
            gitlabURL = (
                "https://gitlab.cern.ch/lhcb-datapkg/" + package + "/-/tree/" + version
            )
            break
    else:
        gitlabURL = None

    outputTypes = [d["FileType"] for d in stepData["OutputFileTypes"]]

    return {
        "stepID": stepData["BKStepID"],
        "application": stepData["ApplicationName"]
        + "/"
        + stepData["ApplicationVersion"],
        "extras": extras,
        "GitLabURL": gitlabURL,
        "options": stepData["OptionFiles"].split(";"),
        "outputTypes": outputTypes,
    }


def getTransformations(production_list):

    # Parse the request parameters

    # Collect all transformations in a single call (it's considerably faster)
    productionIDs = [p["RequestID"] for p in production_list]
    result = TransformationClient().getTransformations(
        {"TransformationFamily": productionIDs}, limit=1000
    )
    allTransformations = defaultdict(list)
    for transformation in _unwrap(result):
        productionID = int(transformation["TransformationFamily"])
        allTransformations[productionID] += [transformationToDict(transformation)]

    return allTransformations


def getPFNsAtSE(lfn_list, SE_config="/Resources/Sites/LCG/LCG.CERN.cern/SE"):
    """  Locate the files at teh site specified """

    pfns = []
    outputSize = 0
    if not lfns:
        return pfns, outputSize

    replicas = _unwrap(DataManager().getReplicas(lfns, getUrl=False))["Successful"]
    # Find the replicas at CERN
    for se in gConfig.getValue(SE_config, []):
        seLFNs = [lfn for lfn in replicas if se in replicas[lfn]]
        if not seLFNs:
            continue
        # Lookup the corresponding PFNs
        result = _unwrap(StorageElement(se).getURL(seLFNs, protocol="root"))
        pfns += list(result["Successful"].values())
        # Compute the size in bytes of the data at CERN
        outputSize += sum(lfnMetadata[lfn]["FileSize"] for lfn in seLFNs)
    return pfns, outputSize


def getAPLFNs(name, year=None, polarity=None, isMC=None, get_pfns=False):
    """
    List the LFNs for a given Analysis production, filtering by year, polarity etc
    """

    # example queries:
    # query = {"RequestID": "69776"}
    # query = {"RequestType": "AnalysisProduction", 'RequestName': 'AnaProd-BsToJpsiPhi', 'SimCondition': 'Beam6500GeV-2015-MagDown-Nu1.6-25ns-Pythia8' }
    # query = {"RequestType": "AnalysisProduction", "RequestName": "AnaProd-RDs"}

    # First getting all productions by name for this Analysis Production
    query = {"RequestType": "AnalysisProduction", "RequestName": name}
    result = ProductionRequestClient().getProductionRequestList(
        0,  # Parent
        "RequestID",  # Sortby
        "DESC",  # Sort order
        0,  # Offset
        0,  # Max results
        query,
    )
    if not result["OK"]:
        raise ValueError(str(result))
    productions = result["Value"]["Rows"]

    # Filtering the productions of interest, based on year and polarity
    prods_to_process = []
    for production in productions:
        # Now retrieving the conditions for each of those productions
        cond_type, conditions = getConditions(production)
        simCondDetail = pickleOrJsonLoads(production["SimCondDetail"])
        prod_year = simCondDetail["configVersion"]

        if not year is None and prod_year != year:
            continue

        if polarity is not None:
            if polarity.lower() == "magdown" and conditions["MagneticField"] != "-1":
                continue
            if polarity.lower() == "magup" and conditions["MagneticField"] != "1":
                continue
        if isMC is not None and not isMC:
            if condType != "daq":
                continue

        prods_to_process.append(production)

    # Now looking for the LFNs
    all_lfns = dict()
    all_pfns = dict()
    for production in prods_to_process:
        allTtransformations = getTransformations([production])
        productionID = production["RequestID"]
        transformations = allTtransformations[productionID]
        lfns = []
        outputSize = 0
        pfnsAtCERN = []
        outputSizeAtCERN = 0

        # Looking up tranformations to find out the LFNs
        if transformations:
            transformationID = transformations[-1]["transformationID"]

            lfnMetadata = _unwrap(
                BookkeepingClient().getProductionFiles(transformationID, "ALL", "ALL")
            )
            # Find the LFNs which were intended as output and their size
            # Mostly needed to ignore LOG files
            expectedFileTypes = transformations[-1]["steps"][-1]["outputTypes"]
            for lfn, metadata in lfnMetadata.items():
                if metadata["FileType"] not in expectedFileTypes:
                    continue
                if not metadata["GotReplica"].upper().startswith("Y"):
                    continue
                lfns.append(lfn)
                outputSize += metadata["FileSize"]

            if get_pfns and lfns:
                replicas = _unwrap(DataManager().getReplicas(lfns, getUrl=False))[
                    "Successful"
                ]
                # Find the replicas at CERN
                for se in gConfig.getValue("/Resources/Sites/LCG/LCG.CERN.cern/SE", []):
                    seLFNs = [lfn for lfn in replicas if se in replicas[lfn]]
                    if not seLFNs:
                        continue
                    # Lookup the corresponding PFNs
                    result = _unwrap(StorageElement(se).getURL(seLFNs, protocol="root"))
                    pfnsAtCERN += list(result["Successful"].values())
                    # Compute the size in bytes of the data at CERN
                    outputSizeAtCERN += sum(
                        lfnMetadata[lfn]["FileSize"] for lfn in seLFNs
                    )

        all_lfns[productionID] = lfns
        all_pfns[productionID] = pfnsAtCERN
    return all_lfns, all_pfns


if __name__ == "__main__":

    # Making sure DIRAC does not interfere
    parser = argparse.ArgumentParser()
    parser.add_argument("-y", "--year", default=None)
    parser.add_argument("-p", "--polarity", choices=["MagDown", "MagUp"], default=None)
    parser.add_argument(
        "--isMC",
        type=bool,
        default=None,
        help="Whether to limit to MC or Data, default is ALL",
    )
    parser.add_argument(
        "--pfns",
        action="store_true",
        default=False,
        help="Return the CERN PFNs for the files mentioned",
    )
    parser.add_argument("prodname", help="Analysis production name")
    args = parser.parse_args()
    all_lfns, all_pfns = getAPLFNs(
        args.prodname,
        year=args.year,
        polarity=args.polarity,
        isMC=args.isMC,
        get_pfns=args.pfns,
    )
    if args.pfns:
        pprint(all_pfns)
    else:
        pprint(all_lfns)
