import json
import os

def is_project_on_disk(siteroot, project, version):
    """ Check if project exist on disk following the LHCb structure """
    if project is None or version is None:
        return False
    ppath = os.path.join(siteroot,  "lhcb", project.upper(), project.upper() + "_" + version)
    return os.path.exists(ppath)


with open("allsteps.json") as f:
    result = json.load(f)

with open("projects_platforms.json") as f:
    platforms = json.load(f)


steps = result['Value']['Records']

used_projects = set()

def process_step(s):
    p = s[2]
    v = s[3]
    u = s[10]
    #print("%s %s %s" % (p, v, u))
    used_projects.add((p, v, u))

def get_platforms(p, v):
    if p is None or v is None:
        return None
    try:
        vers = platforms[p.upper()]
        return vers[v]
    except KeyError:
        return None

#print(steps[0])
for s in steps:
    process_step(s)

missing = []
missing_tofix = []
for p,v, u in sorted(list(used_projects)):
    #print("Checking %s %s" % (p,v))
    ps = get_platforms(p, v)
    #print("Found %s %s %s" % (p,v, ps))
    if ps is None:
        print("Missing %s %s %s" % (p, v, u))
        missing.append((p, v, u))
        if is_project_on_disk("/cvmfs/lhcb.cern.ch/lib", p, v):
            missing_tofix.append((p, v, u))


print("=========== TO FIX:")
for p,v,u in  missing_tofix:
    print("| %s | %s | %s |" % (p, v, u))
